<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpFoundation\Response;

class AuthController extends Controller
{
	/**
	 * @param Request $request
	 * @return JsonResponse
	 */
	public function register(Request $request): JsonResponse
	{
		$request->validate([
			'name' => 'required',
			'email' => 'required|email|unique:users',
			'password' => 'required|min:6'
		]);

		$user = User::create([
			'name' => $request->name,
			'email' => $request->email,
			'password' => Hash::make($request->password)
		]);

		// Auth::guard()->login($user);

		return (new UserResource($user))
			->response()
			->setStatusCode(Response::HTTP_CREATED);
	}

	/**
	 * @param Request $request
	 * @return JsonResponse
	 */
	public function login(Request $request): JsonResponse
	{
		$request->validate([
			'email' => 'required|email',
			'password' => 'required'
		]);

		if (Auth::attempt($request->only('email', 'password'))) {
			return (new UserResource(Auth::user()))
			->response()
			->setStatusCode(Response::HTTP_OK);
		}

		return response()->json(['message' => 'The email or password you have entered is invalid.'], Response::HTTP_UNAUTHORIZED);

		// $user = User::where('email', $request['email'])->first();

		// Auth::guard()->login($user);
	}

	/**
	 * @param Request $request
	 * @return JsonResponse
	 */
	public function logout(Request $request): JsonResponse
	{
		Auth::guard('web')->logout();

		return response()->json(['message' => 'Logout successfully'], Response::HTTP_OK);
	}

	/**
	 * @param Request $request
	 * @return JsonResponse
	 */
	public function user(Request $request): JsonResponse
	{
		return (new UserResource($request->user()))
			->response()
			->setStatusCode(Response::HTTP_OK);
	}
}
